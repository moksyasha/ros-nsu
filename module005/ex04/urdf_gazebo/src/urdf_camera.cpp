#include <ros/ros.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2_ros/transform_broadcaster.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Point32.h>
#include <sensor_msgs/PointCloud.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/point_cloud_conversion.h>

sensor_msgs::PointCloud2 input_pointcloud;
sensor_msgs::PointCloud out_pointcloud;

int height;
int width;

double angular = 0;
double velocity = 0;

void callback(const sensor_msgs::PointCloud2ConstPtr& msg){

  input_pointcloud = *msg;
  height = input_pointcloud.height;
  width = input_pointcloud.width;
  sensor_msgs::convertPointCloud2ToPointCloud(input_pointcloud, out_pointcloud);
  geometry_msgs::Point32 point;

  ROS_INFO("%f", out_pointcloud.points[height*width/2].z);

  point.z = out_pointcloud.points[height*width/2].z;
  
  if (point.z < 0.9)
    velocity = 0;
  else
    velocity = 0.3;
}

int main(int argc, char** argv){
  ros::init(argc, argv, "urdf_camera_init");

  ros::NodeHandle node;
  ros::Subscriber scanSub = node.subscribe<sensor_msgs::PointCloud2> ("/gazyaba/camera1/depth/points", 10, callback);
  ros::Publisher twist_pub = node.advertise<geometry_msgs::Twist>("cmd_vel", 1);
  ros::Rate loop_rate(10);

  while(ros::ok()){

    geometry_msgs::Twist vel_msg;
    vel_msg.angular.z = 0;
    vel_msg.linear.x = velocity;
    twist_pub.publish(vel_msg);
     
    loop_rate.sleep();
    ros::spinOnce();
  }
  
  return 0;
};
