#include <ros/ros.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2_ros/transform_broadcaster.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/LaserScan.h>


sensor_msgs::LaserScan laser_messages;

double angular = 0;
double velocity = 0;

void counterCallback(const sensor_msgs::LaserScan::ConstPtr& scan_msg){

    laser_messages = *scan_msg;
    std::vector<float> laser_ranges;
    laser_ranges = laser_messages.ranges;
    size_t range_size = laser_ranges.size();

    velocity = 0;

    if (laser_ranges[range_size / 2] > 0.5){ 
        velocity = 0.3;
    }

}


int main(int argc, char** argv){
  ros::init(argc, argv, "urdf_wall_init");
  ros::NodeHandle node;
  ros::Subscriber scanSub = node.subscribe<sensor_msgs::LaserScan>("/gazyaba/laser/scan", 10, counterCallback);;
  ros::Publisher twist_pub = node.advertise<geometry_msgs::Twist>("cmd_vel", 1);
  ros::Rate loop_rate(10);
  
  while(ros::ok()){

    geometry_msgs::Twist vel_msg;
    vel_msg.angular.z = 0;
    vel_msg.linear.x = velocity;
    twist_pub.publish(vel_msg);
     
    loop_rate.sleep();
    ros::spinOnce();
  }
  
  return 0;
};
