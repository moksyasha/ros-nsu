#include <ros/ros.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2_ros/transform_broadcaster.h>
#include <geometry_msgs/Twist.h>



int main(int argc, char** argv){
  ros::init(argc, argv, "circle_movement");
  ros::NodeHandle node;
  ros::Publisher twist_pub = node.advertise<geometry_msgs::Twist>("cmd_vel", 1);
  ros::Rate loop_rate(1);

  double i = 0.1;
  while(ros::ok()){

  geometry_msgs::Twist vel_msg;
  vel_msg.angular.z = 0.2;
  vel_msg.linear.x = 0.1 + i;
  twist_pub.publish(vel_msg);
     
  i = -i;
  loop_rate.sleep();
  ros::spinOnce();
  }
  
  
  return 0;
};
