#include "ros/ros.h"
#include "std_msgs/String.h"
#include <geometry_msgs/Twist.h>
#include <sstream>


double angular = 0.0;
double linear = 0.0;


void chatterCallback(const std_msgs::String::ConstPtr& msg)
{
  
  std::string c = msg->data.c_str();

  if (c == ("turn_left")){
    angular = 1.0;
  }
  else if (c == ("turn_right")){
    angular = -1.0;
    }
  else if (c == ("move_forward")){
    linear = 1.0;
  }
  else if (c == ("move_backward")){
    linear = -1.0; 
  }

}

int main(int argc, char **argv)
{

  bool is_pub = false;
  ros::init(argc, argv, "text_to_cmd_vel");

  ros::NodeHandle n;

  ros::Subscriber sub = n.subscribe("cmd_text", 100, chatterCallback);
  ros::Publisher twist_pub = n.advertise<geometry_msgs::Twist>("turtle1/cmd_vel", 100);

  ros::Rate loop_rate(1);

  while (ros::ok())
  {
    geometry_msgs::Twist twist;

    twist.angular.z = 1.4*angular;
    twist.linear.x = 2.0*linear;

    twist_pub.publish(twist);    

    angular = 0.0;
    linear = 0.0;

    ros::spinOnce();

    loop_rate.sleep();
  }
  
  return 0;
}
