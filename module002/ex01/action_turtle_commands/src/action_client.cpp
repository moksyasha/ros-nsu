#include <ros/ros.h>
#include <actionlib/client/simple_action_client.h>
#include <actionlib/client/terminal_state.h>
#include <action_turtle_commands/execute_turtle_commandsAction.h>
#include <iostream>
#include <string>

int main (int argc, char **argv)
{
  ros::init(argc, argv, "action_client");

  // create the action client
  // true causes the client to spin its own thread
  actionlib::SimpleActionClient<action_turtle_commands::execute_turtle_commandsAction> ac("action_server", true);

  ROS_INFO("Waiting for action server to start.");
  // wait for the action server to start
  ac.waitForServer(); //will wait for infinite time

  ROS_INFO("Action server started, sending goal.");
  // send a goal to the action
  action_turtle_commands::execute_turtle_commandsGoal goal;

  std::string commands[5] = { "forward", "turn_right", "forward", "reverse", "forward"};
  int arr_s[5] = {2, 0, 2, 20, 2};
  int arr_angle[5] = {0, 2, 0, 0, 0};

  for(int i = 0; i < 5; i++){
    goal.command = commands[i];
    goal.s = arr_s[i];
    goal.angle = arr_angle[i];

    ac.sendGoal(goal);

    //wait for the action to return
    bool finished_before_timeout = ac.waitForResult(ros::Duration(30.0));

    if (finished_before_timeout)
    {
      actionlib::SimpleClientGoalState state = ac.getState();
      ROS_INFO("Action finished: %s", state.toString().c_str());
    }
    else
      ROS_INFO("Action did not finish before the time out.");
  }

  //exit
  return 0;
}
