#include <ros/ros.h>
#include <actionlib/server/simple_action_server.h>
#include <action_turtle_commands/execute_turtle_commandsAction.h>
#include <geometry_msgs/Twist.h>


class execute_turtle_commandsAction
{
protected:

  ros::NodeHandle nh_;
  actionlib::SimpleActionServer<action_turtle_commands::execute_turtle_commandsAction> as_; // NodeHandle instance must be created before this line. Otherwise strange error occurs.
  std::string action_name_;
  // create messages that are used to published feedback/result
  action_turtle_commands::execute_turtle_commandsFeedback feedback_;
  action_turtle_commands::execute_turtle_commandsResult result_;
  ros::Publisher twist_pub = nh_.advertise<geometry_msgs::Twist>("turtle1/cmd_vel", 1);
  geometry_msgs::Twist twist;
public:

  execute_turtle_commandsAction(std::string name) :
    as_(nh_, name, boost::bind(&execute_turtle_commandsAction::executeCB, this, _1), false),
    action_name_(name)
  {
    as_.start();
  }

  ~execute_turtle_commandsAction(void)
  {
  }

  void executeCB(const action_turtle_commands::execute_turtle_commandsGoalConstPtr &goal)
  {
    // helper variables
    ros::Rate r(1);
    bool success = true;

    // publish info to the console for the user
    ROS_INFO("Executing, command(%s) with metrov %i and angle %i", goal->command.c_str(),
        goal->s, goal->angle); // feedback_.sequence[0], feedback_.sequence[1]);

    std::string c = goal->command.c_str();

    int angular = 0;

    if (c == ("turn_left")){
        angular = 1;
    }
    else if (c == ("turn_right")){
        angular = -1;
    }
    else if (c == ("forward")){
    }
    else
        success = false; 


    // check that preempt has not been requested by the client
    if ((!success) || as_.isPreemptRequested() || !ros::ok())
    {
        ROS_INFO("%s: Preempted", action_name_.c_str());
        // set the action state to preempted
        as_.setPreempted();
        success = false;
        result_.result = false;
        return;
    }    

    twist.angular.z = angular * goal->angle;
    twist.linear.x = goal->s;

    twist_pub.publish(twist);  
    feedback_.odom += goal->s;

    // publish the feedback
    as_.publishFeedback(feedback_);
    // this sleep is not necessary, the sequence is computed at 1 Hz for demonstration purposes
    r.sleep();

    if(success)
    {
      result_.result = true;
      ROS_INFO("%s: Succeeded", action_name_.c_str());
      ROS_INFO("ODO: %i", feedback_.odom);
      // set the action state to succeeded
      as_.setSucceeded(result_);
    }
  }
};


int main(int argc, char** argv)
{
  ros::init(argc, argv, "action_server_node");

  execute_turtle_commandsAction turtle_commands("action_server");
  ros::spin();

  return 0;
}