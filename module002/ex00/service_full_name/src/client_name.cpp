#include "ros/ros.h"
#include "service_full_name/ConcatStrings.h"
#include <string>

int main(int argc, char **argv)
{
  ros::init(argc, argv, "summ_full_name_client");
  if (argc != 4)
  {
    ROS_INFO("usage: summ_full_name_client X Y Z");
    return 1;
  }

  ros::NodeHandle n;
  ros::ServiceClient client = n.serviceClient<service_full_name::ConcatStrings>("summ_full_name");
  service_full_name::ConcatStrings srv;

  srv.request.name = argv[1];
  srv.request.surname = argv[2];
  srv.request.patronymic = argv[3];

  if (client.call(srv))
  {
    ROS_INFO("Concat full name: %s", srv.response.concat.c_str());
  }
  else
  {
    ROS_ERROR("Failed to call service summ_full_name");
    return 1;
  }

  return 0;
}
