#include "ros/ros.h"
#include "service_full_name/ConcatStrings.h"

bool add(service_full_name::ConcatStrings::Request  &req,
         service_full_name::ConcatStrings::Response &res)
{
  
  res.concat = req.name + " " + req.surname + " " + req.patronymic;
  ROS_INFO("request: name=%s, surname=%s, patronymic = %s", req.name.c_str(), req.surname.c_str(), req.patronymic.c_str());
  ROS_INFO("sending back response: [%s]", res.concat.c_str());
  return true;
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "summ_full_name_server");
  ros::NodeHandle n;

  ros::ServiceServer service = n.advertiseService("summ_full_name", add);
  ROS_INFO("Ready to concatenate.");
  ros::spin();

  return 0;
}
