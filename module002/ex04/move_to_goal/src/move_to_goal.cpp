#include "ros/ros.h"
#include <string>
#include <stdlib.h>
#include <iostream>
#include <cmath>
#include <geometry_msgs/Twist.h>
#include "turtlesim/Pose.h"


double theta_sub;
double x1_sub;
double y1_sub;


void getPose(const turtlesim::Pose::ConstPtr& msg) {
  x1_sub = msg->x;
  y1_sub = msg->y;
  theta_sub = msg->theta;// msg->linear_velocity, msg->angular_velocity);
}


int main(int argc, char **argv)
{

  if (argc != 4)
  {
    ROS_INFO("usage: moving X Y THETA");
    return 1;
  }

  bool is_pub = false;
  ros::init(argc, argv, "move_to_goal");

  ros::NodeHandle n;

  ros::Subscriber sub = n.subscribe("turtle1/pose", 100, getPose);
  ros::Publisher twist_pub = n.advertise<geometry_msgs::Twist>("turtle1/cmd_vel", 3);

  ros::Rate loop_rate(1);
  geometry_msgs::Twist twist;
  while (ros::ok())
  {
    

    double x2;
    double y2;
    x2 = double(atof(argv[1]));
    y2 = double(atof(argv[2]));

    double eps = 0.5;

    if((abs(x2 - x1_sub) < eps) && (abs(y2 - y1_sub) < eps)){
      double theta = double(atof(argv[3]));
      twist.angular.z = theta - theta_sub;
      twist.linear.x = 0;
      twist_pub.publish(twist); 
      ROS_INFO("Destination reached");
      loop_rate.sleep();
      break;
    }

    twist.angular.z = atan2(y2 - y1_sub, x2 - x1_sub) - theta_sub;
    twist.linear.x = 0.4 * sqrt(pow(x2-x1_sub, 2) + pow(y2-y1_sub, 2));
    twist_pub.publish(twist); 

    loop_rate.sleep();
    ros::spinOnce();
  }


  return 0;
}
